//import start from 'simple-wsgi';
// import argon2 from 'argon2';
import http from 'http';
import main from './index.js';
//import blobStore from 'content-addressable-blob-store';
//import sqlite from 'knex-sqlite';
import fetch, { Request, Response } from 'node-fetch';
import keypair from 'keypair';
import makeTestApp from './dev/ap-test.js';

const port = process.env.PORT || 8080;
const origin = process.env.ORIGIN || 'http://localhost:' + port;
//const db = sqlite(':memory:');

const password = "$argon2i$v=19$m=4096,t=3,p=1$EWzfNA1vOfvbqTqA/DmdYw$uiYPNWpiYmUMzPgMQYMtmRiyEUjHoxmCRMQyOZytBBY";
// const password = await argon2.hash('password');

const testApp = makeTestApp(fetch);

const app = await main({
	origin,
	password,
	keys: keypair(),
	fetch: {
		fetch: testApp.fetch,
		Request,
		Response
	}
});

const server = http.createServer(testApp(app));

server.listen(port);

console.log('Server listening at', origin)

await new Promise(res => process.once('SIGINT', res));

server.close();