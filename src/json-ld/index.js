import JsonLd from 'jsonld-cached';
import contexts from './contexts/contexts.js';

export default fetch => {
	const jsonLd = JsonLd({ fetch, contexts });
	return Object.fromEntries(['compact', 'expand'].map(m=>[m, jsonLd[m].bind(jsonLd)]));
};