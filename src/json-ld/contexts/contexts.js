import activitystreams from './activitystreams.js';
import security from './security-v1.js';

export default {
	'https://www.w3.org/ns/activitystreams': activitystreams,
	'https://w3id.org/security/v1': security
};
