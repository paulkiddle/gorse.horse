import { expect, jest, test } from '@jest/globals';
import CookieToken from './cookie.js';

test('Cookie token', ()=>{
	const cookieToken = new CookieToken('key', 5000);

	expect(cookieToken.get({
		headers: {
			cookie: 'key=value'
		}
	})).toEqual('value');
	
	expect(cookieToken.get({
		headers: {}
	})).toEqual(undefined);
	

	const res = {
		setHeader: jest.fn()
	};

	cookieToken.set(res, 'new-value');

	expect(res.setHeader.mock.calls).toMatchSnapshot();
});