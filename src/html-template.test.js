import { expect, test } from '@jest/globals';
import { Template } from './html-template.js';
import MockRes from './res.mock.js';

test('Html template', async ()=>{
	const template = Template({ authUrl: '/login' });
	const render = template({ user: { isAnon: true }, title: 'My page' }, 'Page body');

	const res = new MockRes;

	await render(res);

	expect(res.body).toMatchSnapshot();
});

test('Html template, logged in', async ()=>{
	const template = Template({ authUrl: '/login' });
	const render = template({ user: { isAnon: false }, title: 'My page' }, 'Page body');

	const res = new MockRes;

	await render(res);

	expect(res.body).toMatchSnapshot();
});