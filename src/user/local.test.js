import { expect, jest, test } from "@jest/globals";
import User from "./local.js";
import FetchError from './fetch-error.js';
import * as f from 'node-fetch';
import CustomFetch from 'custom-fetch';
import ActivityResource from "./activity-resource.js";
import Errors from "../resources/errors.js";

test('Base user', async()=>{
	const getResource = jest.fn(()=>'r');
	const origin = 'http://example.com'
	const fetch = new CustomFetch(req => new f.Response('{}', { headers: { 'content-type': 'application/json' } }), f);
	const expand = jest.fn(a=>[a])

	const u = new User({ getResource, origin, fetch, expand });

	expect(await u.getResource(origin + '/eg')).toEqual('r');
	expect(await u.getResource('http://other.example/a')).toBeInstanceOf(ActivityResource);
	expect(u.errors).toBeInstanceOf(Errors);

	u.isAnon = false;
	expect(u.errors).toBeInstanceOf(Errors);
})

test('Base user', async()=>{
	const getResource = jest.fn(()=>'r');
	const origin = 'http://example.com'
	const fetch = new CustomFetch(req => new f.Response('{}', { headers: { 'content-type': 'text/plain' } }), f);
	const expand = jest.fn(a=>[a])

	const u = new User({ getResource, origin, fetch, expand });

	await expect(u.getResource('http://other.example/a')).rejects.toBeInstanceOf(FetchError);
})


test('Base user req fail', async()=>{
	const getResource = jest.fn(()=>'r');
	const origin = 'http://example.com'
	const fetch = new CustomFetch(req => new f.Response(null, { status: 404 }), f);
	const expand = jest.fn(a=>[a])

	const u = new User({ getResource, origin, fetch, expand });

	await expect(u.getResource('http://other.example/a')).rejects.toBeInstanceOf(Error);
})