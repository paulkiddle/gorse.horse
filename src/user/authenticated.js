import Errors from '../resources/errors.js';
import BaseUser from './base.js';

export default class Authenticated extends BaseUser {
	isAnon = false;
	#errorList;
	
	constructor({ errorList, id, name }){
		super({
			errorList
		});
		this.#errorList = errorList;
		this.id = id;
		this.name = name;
	}

	get errors(){
		return new Errors(this.name, false, this.#errorList);
	}
}