import { expect, test } from "@jest/globals";
import ActivityResource from "./activity-resource.js";

test('Activity resource', async()=>{
	const data = {
		'a:b': [{ '@value': 'value', '@id': 'id' }]
	};
	const r = new ActivityResource(data)

	expect(r.getId('a:b')).toBe('id');
	expect(r.getValue('a:b')).toBe('value');
	expect(r.data).toEqual(data);
})