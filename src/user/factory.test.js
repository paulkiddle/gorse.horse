import { expect, jest, test } from '@jest/globals';
import Authenticated from './authenticated.js';
import DefaultGetUser, { GetUser } from './factory.js';
import LocalUser from './local.js';

test('Get user', async()=>{
	const gorse = Symbol('Gorse');
	const getToken = jest.fn(() => 'token');
	const auth = { parseToken: jest.fn(()=>false)};
	const getUser = DefaultGetUser({gorse, auth, getToken, getResource: { gorse, auth }, fetch: {} });

	const r = { headers: {} };
	expect((await getUser(r)).isAnon).toBe(true);
	expect(getToken).toHaveBeenCalledWith(r);
	expect(auth.parseToken).toHaveBeenCalledWith('token');

	auth.parseToken = ()=>true;
	expect(await getUser(r)).toBeInstanceOf(LocalUser);
});

test('Get user by signature', async()=>{
	const req = {
		headers: {
			signature: 'signature'
		}
	};

	const gorse = {};
	const getResource = { gorse };
	const parseSignature = jest.fn(()=>'http://example.com/user');
	const fetch = {};

	const getUser = GetUser({ getResource, parseSignature, fetch });

	const user = await getUser(req);

	expect(parseSignature).toHaveBeenCalledWith(req);
	expect(user).toBeInstanceOf(Authenticated)
});