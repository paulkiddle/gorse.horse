import { expect, test } from "@jest/globals";
import Errors from "../resources/errors.js";
import Authenticated from "./authenticated.js";

test('Authenticated user', async()=>{
	expect(new Authenticated({}).errors).toBeInstanceOf(Errors);
})