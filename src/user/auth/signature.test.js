import { expect, test, jest } from "@jest/globals";
import ParseSignature, { assert, collectJson, timeout } from "./signature.js";
import * as sig from 'activitypub-http-signatures';
import keys from './keypair.fixture.js';
import CustomFetch from "custom-fetch";
import { Request, Response } from 'node-fetch';
import { AuthError } from "../factory.js";

test('Auth by signature', async()=>{
	const ownerId = 'http://me.example/paul';
	const publicKeyId = 'http://me.example/paul#key-id';
	const fetch = jest.fn(new CustomFetch(req => new Response(
		JSON.stringify({
			'https://w3id.org/security#publicKey': [{
				'@id': publicKeyId,
				'https://w3id.org/security#owner': [{ '@id': ownerId }],
				'https://w3id.org/security#publicKeyPem': [{
					'@value':  keys.public
				}]
			}]
		}),
		{ headers: { 'content-type': 'application/ld+json' } }
	), {Request, Response}));
	const expand = jest.fn(v=>[v]);

	const parseSignature = ParseSignature({
		fetch,
		expand
	});

	const url = new URL('https://example.com/');
	const method = 'POST';
	const headers = {};

	const signature = sig.sign({
		publicKeyId,
		privateKey: keys.private
	}, {
		url,
		method,
		headers
	});

	const req = { 
		url: url.pathname,
		method,
		headers: {
			signature,
			...headers
		}
	}

	expect(await parseSignature(req)).toBe(ownerId)
});

test('fetch timeout', async()=>{
	const fetch = () => new Promise(()=>{});

	const tFetch = timeout(fetch, 0.001);

	await expect(tFetch()).rejects.toBeInstanceOf(AuthError);
})

test('collect json', async()=>{
	const res = {
		text(){
			return 'invalid'
		},
		headers: {}
	};

	await expect(collectJson(res)).rejects.toBeInstanceOf(AuthError);
})

test('assert', ()=>{
	expect(()=>assert(false, 'msg')).toThrow(AuthError)
})