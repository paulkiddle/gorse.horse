import { AuthError } from '../factory.js';
import * as httpSig from 'activitypub-http-signatures';

const SECONDS = 1000;

export function timeout(fetch, limit = 5 * SECONDS) {
	return async (request, options = {}) => {
		const ac = new AbortController();
		const rejected = new Promise((_,reject) => ac.signal.addEventListener('abort', reject, { once: true }));
		setTimeout(ac.abort.bind(ac), limit);
		return await Promise.race([
			rejected.catch(cause => { throw new AuthError(`The response took longer than the application limit of ${limit}`, { cause }); }),
			fetch(request, { ...options, signal: ac.signal })
		]);
	};
}

// const KB = 1000;
// async function* limit(stream, sizeKb = 100) {
// 	const size = sizeKb * KB;
// 	let s = 0;
// 	for await(const chunk of stream){
// 		s+= chunk.length;
// 		if(s > size) {
// 			throw new AuthError(`The response body was bigger than the application limit of ${sizeKb}KB`);
// 		}
// 		yield chunk;
// 	}
// }

export async function collectJson(res) {
	// const c = [];
	// for await(const chunk of limit(res.body)) {
	// 	console.log(chunk)
	// 	c.push(chunk);
	// }

	// console.log(Buffer.concat(c).toString('utf8'))
	const body = await res.text();

	try {
		return JSON.parse(body);
	} catch(e) {
		throw new AuthError(`The response body is not valid JSON. Its content type is ${res.headers['content-type']}`, { cause: e });
	}
}

const PK = 'https://w3id.org/security#publicKey';
const OWNER = 'https://w3id.org/security#owner';
const PEM = 'https://w3id.org/security#publicKeyPem';

export const assert = (t, msg) => {
	if(!t) {
		throw new AuthError(msg);
	}

	return t;
};

export default function ParseSignature({ fetch, expand }) {
	const timedFetch = timeout(fetch);

	return async function parseSignature(req) {
		const { headers, signature, keyId/*, algorithm*/ } = httpSig.parse(req.headers.signature);
		const keyRes = await timedFetch(keyId, { headers: { accept: 'application/ld+json, application/json' } });
		
		assert(keyRes.ok, `The request for the private key returned status ${keyRes.statusCode}`);

		const json = await collectJson(keyRes);
		const e = await expand(json);
		const [expanded] = e;

		const k = expanded[PK];
		assert(k, 'Fetched resource has no ' + PK);

		const keyOb = k.find(o=>o['@id'] === keyId);
	
		assert(keyOb, 'no key with id ' + keyId);

		const owner = keyOb[OWNER]?.find(o=>keyId.startsWith(o['@id']))?.['@id'];

		assert(owner, 'Key ID string must start with owner id string');

		const pem = keyOb[PEM]?.[0]['@value'];

		assert(pem, 'No PEM found on key object');

		const request = {
			target: req.url,
			method: req.method,
			headers: Object.fromEntries(headers.slice(1).map(h=>[h, req.headers[h]]))
		};

		assert(httpSig.verify(signature, pem, request), 'The signature verification failed');

		return owner;
	};
}