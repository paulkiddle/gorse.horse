import User from './base.js';
import Authenticated from './authenticated.js';
import LocalUser from './local.js';
import SignedFetch from 'signed-fetch';
import ParseSignature from './auth/signature.js';

export class AuthError extends Error {}

export function GetUser({ getResource, getToken, origin, fetch, expand, privateKey, parseSignature }) {
	const errorList = [];
	const { gorse, auth } = getResource;
	const anon = new User({ errorList });
	const gorseFetch = new SignedFetch({
		keyId: gorse.keyId,
		privateKey
	}, fetch);
	const authu = new LocalUser({ gorse, getResource, origin, expand, fetch: gorseFetch, errorList, id: gorse.id, name: gorse.name });

	return async function getUser(req) {
		if(req.headers.signature) {
			const owner = await parseSignature(req);
			const u = new Authenticated({ errorList, id: owner, name: owner });
			await u.errors.append('Signature authenticated successfuly.');
			return u;
		}
		const token = getToken(req);
		if(auth.parseToken(token)) {
			return authu;
		}

		return anon;
	};
}

export default function({ fetch, expand, ...args }){
	const parseSignature = ParseSignature({ fetch: fetch.fetch, expand });
	return GetUser({ fetch, parseSignature, ...args });
}