export default class ActivityResource {
	#data;

	isResource = true;

	constructor(data) {
		this.#data = data;
	}

	get(...fields) {
		return fields.reduce((data, field) => data?.[field]?.[0], this.#data);
	}

	getValue(...fields) {
		return this.get(...fields)?.['@value'];
	}

	getId(...fields) {
		return this.get(...fields)?.['@id'];
	}

	get data(){
		return this.#data;
	}
}