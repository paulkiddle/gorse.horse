import Errors from '../resources/errors.js';
import ActivityResource from './activity-resource.js';
import Authenticated from './authenticated.js';
import FetchError from './fetch-error.js';

export default class LocalUser extends Authenticated {
	#getResource;
	#origin;
	#fetch;
	#expand;
	#errorList;

	constructor({ fetch, getResource, origin, expand, errorList, id, name }) {
		super({ errorList, id, name });
		this.#getResource = getResource;
		this.#origin = origin;
		this.#fetch = fetch;
		this.#expand = expand;
		this.#errorList = errorList;
	}

	async getResource(url){
		if(url.startsWith(this.#origin)) {
			return await this.#getResource(this, url);
		} else {
			return await this.getRemoteResource(url);
		}
	}

	async getRemoteResource(url) {
		const res = await this.#fetch(url, { headers: { accept: 'application/json' } });
		if(!res.ok) {
			throw new FetchError('Fetch failed with ' + res.statusCode, res);
		}

		if(res.headers.get('content-type') !== 'application/json') {
			throw new FetchError(`Returned type ${res.headers.get('content-type')}`, res);
		}

		const data = await res.json();
		const [record] = await this.#expand(data);
		return new ActivityResource(record);
	}

	get errors(){
		return new Errors(this.name, true, this.#errorList);
	}
}