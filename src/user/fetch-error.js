export default class FetchError extends Error {
	constructor(message, response, options){
		super(message, options);
		this.response = response;
	}
}