import Errors from '../resources/errors.js';

export default class User {
	#errorList;
	isAnon = true;

	constructor({ errorList = [] } = {}) {
		this.#errorList = errorList;
	}

	get errors(){
		return new Errors('Anon user', false, this.#errorList);
	}
}
