import html from 'encode-html-template-tag';
import { pipeline } from 'stream/promises';

export function Template({ authUrl }) {
	return function ({ user, title }, body) {
		const auth = user.isAnon ? html`<a href="${authUrl}">Log in</a>` : 'Authenticated';
		const document = html`<!doctype html><html><head><title>${title}</title></head><body>${auth}<main>${body}`;
		
		return res => pipeline(document.generate(), res);
	};
}