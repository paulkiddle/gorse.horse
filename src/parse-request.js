import path from 'path';
import mime from 'mime-types';
import accepts from 'accepts';
import contentType from 'content-type';

export default function parseRequest(origin, req) {
	const url = new URL(req.url, origin);
	const extName = path.extname(url.pathname);
	if(extName) {
		url.pathname = url.pathname.slice(0, -extName.length);
	}

	const extType = extName && mime.lookup(extName);
	const accept = req.headers?.accept || '*/*;q=0.1';

	const acceptTypes = extType || accept;

	const contentTypeHeader = req.headers?.['content-type'];

	return {
		url,
		accepts: accepts({ headers: { ...req.headers, accept: acceptTypes } }),
		method: req.method,
		[Symbol.asyncIterator]: req[Symbol.asyncIterator]?.bind(req),
		contentType: contentTypeHeader && contentType.parse(contentTypeHeader)?.type
	};
}
