import html from 'encode-html-template-tag';

const authForm = error => html`
	<h1>Log in</h1>
	<form method="POST">
		${error||''}
		<label>Password <input name="password"></label>
	</form>`;
export default authForm;

export const parse = body => body.get('password');