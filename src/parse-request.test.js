import { expect, test } from '@jest/globals';
import parseRequest from './parse-request.js';

test('parse request', async()=>{
	const req = {
		url: '/test.jpg',
		headers: {
			accept: 'application/json',
			'content-type': 'application/json'
		}
	};
	const origin = 'http://example.com';

	expect(parseRequest(origin, req)).toMatchSnapshot();
});

test('parse request no type info', async()=>{
	const req = {
		url: '/test'
	};
	const origin = 'http://example.com';

	expect(parseRequest(origin, req)).toMatchSnapshot();
});