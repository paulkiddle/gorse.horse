import { Writable } from 'stream';

export default class MockRes extends Writable {
	body = '';
	headers = {};

	constructor() {
		super({
			write: (chunk, encoding, cb) => {
				this.body += chunk;
				cb();
			}
		});
	}

	setHeader(k, v){
		this.headers[k]=v;
	}
}