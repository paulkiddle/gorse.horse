import GetUser from './user/factory.js';
import ResourceFactory from './resources/factory.js';
import { ActivitypubWebfinger } from 'webfinger-handler';
import { AssetMap, assetRequestHandler } from 'liquid-assets';
import parseRequest from './parse-request.js';
import ControllerFactory from './controllers/router.js';
import { Template } from './html-template.js';
import CookieToken from './cookie.js';
import accepts from 'accepts';
import JsonLd from './json-ld/index.js';

export function errorCheck(fn, log){
	return async function(req, res) {
		try {
			if(!await fn(req, res)) {
				log(new Error('Request was not served.'));
				res.statusCode = 404;
				res.end('You\'re so lost you can\'t even find the 404 page.');
			}
		} catch(e) {
			log(e);
			res.statusCode = 500;
			res.end('A very bad error happened, sorry!');
		}
		return true;
	};
}

export function GetWebfingerActor({ gorse }){
	return async resource => {
		if(resource.host === new URL(gorse.id).host && resource.user === gorse.username) {
			return gorse.id;
		}

		return null;
	};
}

function makeWebfingerHandler({ gorse }){
	const getWebfingerActor = GetWebfingerActor({ gorse });
	return ActivitypubWebfinger(getWebfingerActor);
}

export function combine(...middlewares) {
	return async function(req, res) {
		for(const fn of middlewares) {
			if(await fn(req, res)) {
				return true;
			}
		}

		return false;
	};
}

export function App ({ getUser, getResource, getController, origin }) {
	const errorController = getController.error;

	return async function(req, res) {
		const user = await getUser(req);
		const parsedRequest = {
			user,
			...parseRequest(origin, req)
		};

		try {
			const resource = await getResource(user, parsedRequest.url);
			const controller = getController(resource, parsedRequest);
			await controller(res);
		} catch(e) {
			user.errors.append(e);
			const controller = errorController(e, { ...parsedRequest, accepts: accepts(req) });
			await controller(res);
		}

		return true;
	};
}

const YEAR = 31557600;

export default async function ({ origin, password, fetch, keys }) {
	const cookieKey = 'auth';
	const assetBase = new URL('/assets/', origin);
	const assets = new AssetMap(assetBase);

	const getResource = await ResourceFactory({ origin, assets, password, ttl: YEAR, publicKey: keys.public });
	const { gorse, auth } = getResource;

	const { compact, expand } = JsonLd(fetch.fetch);

	const authUrl = getResource.urlFor(auth);
	const htmlTemplate = Template({ authUrl });
	const token = new CookieToken(cookieKey, YEAR);
	const getController = ControllerFactory({ setToken: token.set.bind(token), htmlTemplate, compact, origin });
	const getUser = GetUser({ origin, expand, getResource, getToken: token.get.bind(token), fetch, privateKey: keys.private });

	const webfinger = makeWebfingerHandler({ gorse });
	const assetServer = assetRequestHandler(assets);


	const app = App({ getUser, getResource, getController, origin });
	
	return errorCheck(
		combine(webfinger, assetServer, app),
		console.error.bind(console)
	);
}

