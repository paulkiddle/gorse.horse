import cookie from 'cookie';

export default class CookieToken {
	#key;
	#ttl;

	constructor(key, ttl) {
		this.#key = key;
		this.#ttl = ttl;
	}

	get(req) {
		const cookies = cookie.parse(req.headers.cookie || '');
		const token = cookies[this.#key];
		return token;
	}

	set(res, token) {
		res.setHeader('Set-Cookie', cookie.serialize(this.#key, token, { path: '/', maxAge: this.#ttl }));
	}
}