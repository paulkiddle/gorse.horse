import index, { App, errorCheck, GetWebfingerActor, combine } from '../src/index.js';
import { expect, jest, test } from '@jest/globals';
import User from './user/base.js';

class MockResponse {
	end = jest.fn();
}

function appArgs({ user, resource }){
	const serializer = {
		serialize: jest.fn()
	};

	const controller = {	};

	const getUser = jest.fn(()=>user);
	const getSerializer = jest.fn(()=>serializer);
	const getResource = jest.fn(()=>resource);
	getSerializer.serializer = serializer;
	const getController = jest.fn(()=>controller);
	getController.error = jest.fn(()=>jest.fn());
	const webfinger = async () => false;

	return { getUser, getResource, getController, origin: 'http://example.com', webfinger, keys: {}, fetch:{} };
}

test('Error check ok', async ()=>{
	const res = new MockResponse;
	const log = jest.fn();
	const wrapped = errorCheck(
		()=>true,
		log
	);

	expect(await wrapped({}, res)).toBe(true);

	expect(log).not.toHaveBeenCalled();
	expect(res.end).not.toHaveBeenCalled();
});

test('Error check catches errors', async ()=>{
	const res = new MockResponse;
	const log = jest.fn();
	const error = new Error('Mock error');
	const wrapped = errorCheck(
		()=>{throw error;},
		log
	);

	expect(await wrapped({}, res)).toBe(true);

	expect(log).toHaveBeenCalledWith(error);
	expect(res.end).toHaveBeenCalledWith('A very bad error happened, sorry!');
	expect(res.statusCode).toBe(500);
});

test('Error check catches unhandled requests', async ()=>{
	const res = new MockResponse;
	const log = jest.fn();
	const wrapped = errorCheck(
		()=>false,
		log
	);

	expect(await wrapped({}, res)).toBe(true);

	expect(log).toHaveBeenCalledWith(expect.any(Error));
	expect(res.end.mock.calls).toMatchSnapshot();
	expect(res.statusCode).toBe(404);
});

test('App gets resource and serializes it', async() => {
	const user = new User({ errorList: [] });
	const resource = Symbol('resource');
	const args = appArgs({ user, resource });
	const { getUser, getResource} = args;
	
	const app = App({ ...args });

	const req = {
		url: '/'
	};
	const res = Symbol('response');

	await app(req, res);

	expect(getUser).toHaveBeenCalledWith(req);
	expect(getResource).toHaveBeenCalledWith(user, new URL('http://example.com/'));
});

test('Create app instance', async ()=>{
	expect(await index({ origin: 'http://example.com', keys: {}, fetch:{} })).toBeInstanceOf(Function);
});

test('Combines middlewares', async()=>{
	const mw = [
		jest.fn(() => false),
		jest.fn(() => false),
	];
	const req = 'Req';
	const res = 'Res';

	expect(await combine(...mw)(req, res)).toBe(false);
	expect(mw[0]).toHaveBeenCalledWith(req, res);
	expect(mw[1]).toHaveBeenCalledWith(req, res);

	mw.unshift(jest.fn(()=>true));

	const [req2, res2] = ['Req2', 'Res2'];

	expect(await combine(...mw)(req2, res2)).toBe(true);
	expect(mw[0]).toHaveBeenCalledWith(req2, res2);
	expect(mw[1]).not.toHaveBeenCalledWith(req2, res2);
	expect(mw[2]).not.toHaveBeenCalledWith(req2, res2);
});

test('GetWebfingerActor', async()=>{
	const gorse = { id: 'http://gorse.example/@gorse', username: 'gorse' };
	const gwa = GetWebfingerActor({ gorse });
	expect(await gwa({ host: 'gorse.example', user: 'gorse' })).toEqual(gorse.id);
	expect(await gwa({ host: 'example.com', user: 'gorse' })).toEqual(null);
	expect(await gwa({ host: 'gorse.example', user: 'horse' })).toEqual(null);
});
