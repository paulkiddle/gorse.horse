import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import Resource from './resource.js';

export default class Auth extends Resource {
	#hash;
	#ttl;

	constructor(hash, ttl) {
		super();
		this.#hash = hash;
		this.#ttl = ttl;
	}

	async authenticate(password) {
		if(await argon2.verify(this.#hash, password)) {
			const token = jwt.sign({ authenticated: true }, this.#hash, { expiresIn: this.#ttl });
			return token;
		} else {
			return false;
		}
	}

	parseToken(token) {
		try {
			const object = token && jwt.verify(token, this.#hash);
			return object?.authenticated;
		} catch(e) {
			return false;
			// NB: It's impossible to detect whether the token is invalid or some other error occured
			// if(!(e instanceof jwt.JsonWebTokenError)) {
			// 	throw e;
			// }
		}
	}
}
