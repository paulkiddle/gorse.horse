import { asset } from 'liquid-assets';
import Resource from './resource.js';

export const avatar = asset('./gorse.png', import.meta.url);

export default class Gorse extends Resource {
	constructor(id, { avatar, publicKey }={}){
		super();
		this.id = id;
		this.avatar = avatar;
		this.publicKey = publicKey;
		this.keyId = id + '#main-key';
	}

	name = 'Gorse';
	about = 'This is the home page of Gorse the horse.';
	username = 'gorse';
	avatarType = avatar.type;
}