import { expect, jest, test } from "@jest/globals";
import RemoteForm from "./remote-form.js";

test('Remote form', async ()=>{
	const getResource = jest.fn(a=>'resource');
	const resourceForm = new RemoteForm(getResource);

	expect(await resourceForm.get('a://b')).toBe('resource');
	expect(getResource).toHaveBeenCalledWith('a://b');
})