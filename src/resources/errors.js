import { NotAuthenticatedError } from './factory.js';

export default class Errors {
	#errors;
	#userInfo;
	#canView;

	constructor(userInfo, canView, errors) {
		this.#errors = errors;
		this.#userInfo = userInfo;
		this.#canView = canView;
	}

	get empty(){
		this.#assertView();
		return this.#errors.length < 1;
	}

	#assertView(){
		if(!this.#canView) {
			throw new NotAuthenticatedError('You must be logged in to view this data');
		}
	}

	[Symbol.iterator](){
		this.#assertView();
		return this.#getItems();
	}

	*#getItems(){
		yield* this.#errors;
	}

	append(e){
		this.#errors.unshift({
			stack: typeof e === 'string' ? e : e.stack,
			user: this.#userInfo,
			time: new Date().toUTCString()
		});
	}

	map(fn) {
		this.#assertView();
		return this.#map(fn);
	}

	*#map(fn) {
		for(const error of this.#getItems()) {
			yield fn(error);
		}
	}
}