export default class RemoteForm {
	#getResource;

	constructor(getResource) {
		this.#getResource = getResource;
	}

	async get(url) {
		return this.#getResource(url);
	}
}