import Auth from './auth.js';
import Gorse, { avatar } from './gorse.horse.js';
import RemoteForm from './remote-form.js';
import Errors from './errors.js';

export class RequestError extends Error {}
export class ResourceNotFoundError extends RequestError {}
export class NotAuthenticatedError extends RequestError {}

export default async function ResourceFactory({ origin, assets, password, ttl, publicKey }) {
	const gorseAvatar = await assets.getUrl(avatar);
	const auth = new Auth(password, ttl);

	const { href } = new URL('/', origin);
	const gorse = new Gorse(href, { avatar: gorseAvatar, publicKey });

	async function getResource(user, uri) {
		switch(uri.pathname) {
		case '/':
		case `/@${gorse.username}`:
			return gorse;
		case '/auth':
			return auth;
		case '/remote':
			if(user.isAnon) {
				throw new NotAuthenticatedError('You must be logged in to visit this page.');
			}
			return new RemoteForm(user.getRemoteResource.bind(user));
		case '/errors':
			return user.errors;
		}

		throw new ResourceNotFoundError(`No resource exists at ${uri.pathname}`);
	}

	getResource.gorse = gorse;
	getResource.auth = auth;
	getResource.urlFor = function urlFor(resource) {
		if(resource instanceof Auth) {
			return '/auth';
		}
		if (resource instanceof Gorse) {
			return '/';
		}
		if(resource instanceof RemoteForm) {
			return '/remote';
		}
		if(resource instanceof Errors) {
			return '/errors';
		}

		throw new Error('Can\'t resolve URL for resource ' + resource);
	};

	return getResource;
}
