import { expect, test } from '@jest/globals';
import Gorse from './gorse.horse.js';

test('Get data', ()=>{
	const g = new Gorse('http://example.com', { avatar: 'http://avatar.example/', publicKey: 'pkey' });

	expect(g).toMatchSnapshot();
})