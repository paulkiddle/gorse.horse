import { expect, test } from "@jest/globals";
import Errors from "./errors.js";
import { NotAuthenticatedError } from "./factory.js";

test('Errors', ()=>{
	const e = new Errors('User', false, []);

	expect(()=>e[Symbol.iterator]()).toThrow(NotAuthenticatedError);

	const list = [1,2];
	const e2 = new Errors('User', true, list);
	e2.append('Test')

	expect(Array.from(e2)).toEqual(list);
});