import { expect } from '@jest/globals';
import User from '../user/base.js';
import LocalUser from '../user/local.js';
import Errors from './errors.js';
import ResourceFactory, { NotAuthenticatedError, ResourceNotFoundError } from './factory.js';
import RemoteForm from './remote-form.js';

test('Resource factory makes resource', async()=>{
	const assets = { getUrl(){ return 'http://example.com/image'} }
	const getResource = await ResourceFactory({ origin: 'http://example.com', assets });
	const {gorse, auth, urlFor} = getResource;
	let user = new LocalUser({});
	expect(await getResource(user, new URL('file:///'))).toBe(gorse);
	expect(await getResource(user, new URL('file:///@gorse'))).toBe(gorse);
	expect(await getResource(user, new URL('file:///auth'))).toBe(auth);
	expect(await getResource(user, new URL('file:///remote'))).toBeInstanceOf(RemoteForm);
	expect(await getResource(user, new URL('file:///errors'))).toBeInstanceOf(Errors);

	user = new User({});
	await expect(getResource({ isAnon: true }, new URL('file:///remote'))).rejects.toBeInstanceOf(NotAuthenticatedError);
	await expect(getResource(user, new URL('file:///about'))).rejects.toBeInstanceOf(ResourceNotFoundError);

	expect(urlFor(gorse)).toBe('/');
	expect(urlFor(auth)).toBe('/auth');
	expect(urlFor(Object.create(RemoteForm.prototype))).toBe('/remote')
	expect(urlFor(Object.create(Errors.prototype))).toBe('/errors')
	expect(urlFor).toThrowError();
})