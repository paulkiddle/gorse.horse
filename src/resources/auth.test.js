import { expect, test } from "@jest/globals";
import Auth from "./auth.js";

test('Auth', async()=>{
	const hash = "$argon2i$v=19$m=4096,t=3,p=1$EWzfNA1vOfvbqTqA/DmdYw$uiYPNWpiYmUMzPgMQYMtmRiyEUjHoxmCRMQyOZytBBY";
	const auth = new Auth(hash, 1000);
	
	expect(await auth.authenticate('wrong')).toBe(false);

	const token = await auth.authenticate('password');
	expect(token).toBeTruthy();

	expect(auth.parseToken(token)).toBe(true);
	expect(auth.parseToken('wrong')).toBe(false);
})