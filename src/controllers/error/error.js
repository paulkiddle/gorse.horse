import html from 'encode-html-template-tag';
import { NotAuthenticatedError, ResourceNotFoundError } from '../../resources/factory.js';
import { AuthError } from '../../user/factory.js';
import { UnsupportedMediaTypeError } from '../auth/auth.js';
import { default as NotAcceptableError } from '../general/serializer/not-acceptible.js';
import { NotImplementedError } from '../router.js';

const setStatus = (res, title, status) => {
	res.statusCode = status ?? 500;
	res.statusText = title;
};

function nobodySerializer (res, e, user) {
	const { title, status } = getBody(e, user);
	setStatus(res, title, status);
	res.end();
}

function errorBody(title, message) {
	return html`<h1>${title}</h1>${message}`;
}

export const SerializeHtml = ({ htmlTemplate }) => async function serializeHtml(res, e, user){
	const { title, body, status } = getBody(e, user);
	setStatus(res, title, status);
	const content = errorBody(title, body);
	const render = htmlTemplate({ title, user }, content);
	await render(res);
};

export function getBody(error, user){
	if(error instanceof ResourceNotFoundError) {
		return {
			title: 'Page Not Found',
			body: error.message,
			status: 404
		};
	}

	const isUser = user && !user.isAnon;

	if(error instanceof NotAuthenticatedError) {
		return {
			title: isUser ? 'Action Forbidden' : 'User Not Authenticated',
			body: error.message,
			status: isUser ? 403 : 401
		};
	}

	if(error instanceof AuthError) {
		return {
			title: 'Signature Authentication Failed',
			body: error.message,
			status: 401
		};
	}

	if(error instanceof NotAcceptableError) {
		return {
			title: 'Content Type Not Acceptable',
			body: html`
				<p>${error.message}</p>
				<p>The available content types are:</p>
				<ul>
					${error.types.map(t=>html`<li>${t}</li>`)}
				</ul>
			`,
			headers: {
			//	Alternates: error.types.map(t => `{ "" }`)
			},
			status: 406
		};
	}

	if(error instanceof UnsupportedMediaTypeError) {
		return {
			title: 'Unsupported Media Type',
			body: error.message,
			status: 415
		};
	}

	if(error instanceof NotImplementedError) {
		return {
			title: 'Not Implemented',
			body: error.message,
			status: 501
		};
	}

	user.errors.append(error);

	return {
		title: isUser ? error.toString() : 'Internal Server Error',
		body: isUser ? html`<pre>${error.stack}</pre>` : 'An unexpected error has occured and the resource cannot be displayed.'
	};
}

export const getSerializer = (accepts, htmlTemplate) => accepts?.type(['text/html']) ? SerializeHtml({ htmlTemplate }) : nobodySerializer;

export default function ErrorController({ htmlTemplate }) {
	function error(e, { user, accepts }) {
		const serialize = getSerializer(accepts, htmlTemplate);

		return async function errorController (res) {
			await serialize(res, e, user);
		};
	}

	return error;
}
