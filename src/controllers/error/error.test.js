import { expect, test, jest } from "@jest/globals";
import ErrorController, { getBody, getSerializer, SerializeHtml } from "./error.js";
import MockRes from '../../res.mock.js';
import { NotAuthenticatedError, ResourceNotFoundError } from "../../resources/factory.js";
import { NotImplementedError } from "../router.js";
import { default as NotAcceptableError } from "../general/serializer/not-acceptible";
import { UnsupportedMediaTypeError } from "../auth/auth.js";
import accepts from "accepts";
import { AuthError } from "../../user/factory.js";
import Authenticated from "../../user/authenticated.js";
import User from "../../user/base.js";

test('Error controller', async()=>{
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, body));
	const getErrorController = ErrorController({ htmlTemplate });

	const e = new Error();
	const controller = getErrorController(e, { user: new User });

	const res = new MockRes;

	await controller(res);

	expect(res).toMatchSnapshot();
})

test('Error controller get body', async() => {
	expect(getBody(new ResourceNotFoundError('Resource not found'))).toMatchSnapshot('404');
	expect(getBody(new NotAuthenticatedError('Not authenticated'))).toMatchSnapshot('403');
	expect(getBody(new NotAuthenticatedError('Not authenticated'), { isAnon: false })).toMatchSnapshot('403');
	expect(getBody(new AuthError('Auth error'))).toMatchSnapshot('401');
	expect(getBody(new NotImplementedError('Not implemented'))).toMatchSnapshot('501');
	expect(getBody(new UnsupportedMediaTypeError('Unsupported media type'))).toMatchSnapshot('415');
	expect(getBody(new NotAcceptableError('Cant accept that type', ['text/html', 'application/json']))).toMatchSnapshot('506')
	expect(getBody(new Error('Misc error'), new User({ errorList: [] }))).toMatchSnapshot('500');
	expect(getBody(new Error('Misc error'), new Authenticated({ errorList: [] }))).toMatchSnapshot('500');
})

test('Serialize HTML', async() => {
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, props, body))
	const serialize = SerializeHtml({ htmlTemplate });
	const res = new MockRes;
	await serialize(res, new Error, new User({ errorList: [] }));
	expect(render.mock.calls).toMatchSnapshot();
});

test('Get nobody serializer', async() => {
	const serializer = getSerializer(null, jest.fn());
	expect(serializer.name).toBe('nobodySerializer');
});


test('Get html serializer', async() => {
	const serializer = getSerializer(accepts({ headers: { accept: 'text/html' } }), jest.fn());
	expect(serializer.name).toBe('serializeHtml');
});