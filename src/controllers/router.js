import Auth from '../resources/auth.js';
import DefaultController from './general/general.js';
import Gorse from '../resources/gorse.horse.js';
import AuthController from './auth/auth.js';
import ErrorController from './error/error.js';
import RemoteForm from '../resources/remote-form.js';
import RemoteResourceController from './remote-form/remote-form.js';
import Errors from '../resources/errors.js';

export class NotImplementedError extends Error { }

export default function ControllerFactory({ htmlTemplate, origin, setToken, compact }) {
	const getDefaultController = DefaultController({ htmlTemplate, compact });
	const getAuthController = AuthController({ setToken, htmlTemplate });
	const getErrorController = ErrorController({ htmlTemplate });

	function getController(resource, req) {
		if(resource instanceof Gorse || resource instanceof Errors) {
			return getDefaultController(resource, req);
		} else if (resource instanceof Auth) {
			return getAuthController(resource, req);
		} else if (resource instanceof RemoteForm) {
			return RemoteResourceController({ htmlTemplate, origin }, resource, req);
		}

		throw new NotImplementedError('The server does not know how to display the resource you are trying to access.');
	}

	getController.error = getErrorController;

	return getController;
}
