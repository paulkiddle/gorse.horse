import { expect, jest, test } from "@jest/globals";
import RemoteResourceController from "./remote-form.js";
import MockRes from '../../res.mock.js';
import FetchError from '../../user/fetch-error.js';

test('Remote form controller', async ()=>{
	const form = { get: jest.fn() };
	const htmlTemplate = jest.fn((args, body) => (res)=>res.end(JSON.stringify(args)));
	const req = {
		url: new URL('http://example.com/')
	}
	const controller = RemoteResourceController({ htmlTemplate, origin: 'http://example.com' }, form, req);

	const res = new MockRes;

	await controller(res);

	expect(res).toMatchSnapshot();
})

test('Remote form controller with query request', async ()=>{
	const form = { get: jest.fn() };
	const htmlTemplate = jest.fn((args, body) => (res)=>res.end(JSON.stringify(args)));
	const req = {
		url: new URL('http://example.com/?url=http://horse.example/resource')
	}
	const controller = RemoteResourceController({ htmlTemplate }, form, req);

	const res = new MockRes;

	await controller(res);

	expect(res).toMatchSnapshot();
})

test('Remote form controller renders fetch error', async ()=>{
	const form = { get: jest.fn(()=>{ throw new FetchError('Mock fetch error', { text() { return 'response body' }}) }) };
	const htmlTemplate = jest.fn((args, body) => (res)=>res.end(JSON.stringify(args)));
	const req = {
		url: new URL('http://example.com/?url=http://horse.example/resource')
	}
	const controller = RemoteResourceController({ htmlTemplate }, form, req);

	const res = new MockRes;

	await controller(res);

	expect(htmlTemplate.mock.calls).toMatchSnapshot();
})

test('Remote form controller throws other error', async ()=>{
	const e = new Error('General error')
	const form = { get: jest.fn(()=>{ throw e}) };
	const htmlTemplate = jest.fn((args, body) => (res)=>res.end(JSON.stringify(args)));
	const req = {
		url: new URL('http://example.com/?url=http://horse.example/resource')
	}
	const controller = RemoteResourceController({ htmlTemplate }, form, req);

	const res = new MockRes;

	await expect(controller(res)).rejects.toBe(e);

	expect(htmlTemplate.mock.calls).toMatchSnapshot();
});

test('Remote form controller redirects to local url', async ()=>{
	const origin = 'http://example.com';
	const form = { };
	const htmlTemplate = jest.fn();
	const req = {
		url: new URL('http://example.com/?url=http://example.com/resource')
	}
	const controller = RemoteResourceController({ htmlTemplate, origin }, form, req);

	const res = new MockRes;

	await controller(res);

	expect(res).toMatchSnapshot();
});
