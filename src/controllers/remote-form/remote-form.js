import html from 'encode-html-template-tag';
import FetchError from '../../user/fetch-error.js';

export default function RemoteResourceController({ htmlTemplate, origin }, form, req) {
	async function getRemoteResource(url) {
		try {
			return html`<pre>${JSON.stringify(await form.get(url), null, 2)}</pre>`;
		} catch(e) {
			if(e instanceof FetchError) {
				return html`<h1>${e.message}</h1><pre>${await e.response.text()}</pre>`;
			}

			throw e;
		}
	}

	return async function remoteResourceController(res) {
		const urlStr = req.url.searchParams.get('url') ?? '';
		const url =  urlStr && new URL(urlStr);

		if(url?.origin === origin) {
			res.statusCode = 308;
			res.setHeader('Location', url);
			res.end();
			return;
		}

		const remoteResource = url && await getRemoteResource(url);

		const render = htmlTemplate({ title: 'View Remote Resource', user: req.user }, html`<form>
			<label>Resource URL<input name=url value="${url}"></label>
			<button>Go</button>
		</form>
		${remoteResource}`);

		await render(res);
	};
}