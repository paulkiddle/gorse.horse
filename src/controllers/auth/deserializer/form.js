export default async function deserializeForm(stream){
	let body = '';

	for await(const chunk of stream) {
		body += chunk;
	}

	return new URLSearchParams(body);
}

deserializeForm.type = 'form/x-www-form-urlencoded';