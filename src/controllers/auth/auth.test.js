import { expect, test, jest } from "@jest/globals";
import AuthControllerFactory, { UnsupportedMediaTypeError } from "./auth.js";
import MockRes from '../../res.mock.js';

test('Auth controller GET', async()=>{
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, body));
	const setToken = jest.fn();

	const getAuthController = AuthControllerFactory({ htmlTemplate, setToken });

	const auth = {};
	const req = {};

	const controller = getAuthController(auth, req);

	const res = new MockRes;

	await controller(res);

	expect(render.mock.calls).toMatchSnapshot();
})


test('Auth controller POST', async()=>{
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, body));
	const setToken = jest.fn();

	const getAuthController = AuthControllerFactory({ htmlTemplate, setToken });

	const auth = {
		authenticate: jest.fn(()=>'token')
	};

	const req = {
		method: 'POST',
		async *[Symbol.asyncIterator](){
			yield 'password=123'
		}
	};

	const controller = getAuthController(auth, req);

	const res = new MockRes;

	await controller(res);

	expect(auth.authenticate).toHaveBeenCalledWith('123');
	expect(setToken).toHaveBeenCalledWith(res, 'token');
	expect(res.headers).toMatchSnapshot();
	expect(render.mock.calls).toMatchSnapshot();
});

test('Auth controller POST invalid password', async()=>{
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, body));
	const setToken = jest.fn();

	const getAuthController = AuthControllerFactory({ htmlTemplate, setToken });

	const auth = {
		authenticate: jest.fn(()=>false)
	};

	const req = {
		method: 'POST',
		async *[Symbol.asyncIterator](){
			yield 'password=123'
		}
	};


	const controller = getAuthController(auth, req);
	const res = new MockRes;

	await controller(res);

	expect(auth.authenticate).toHaveBeenCalledWith('123');
	expect(setToken).not.toHaveBeenCalled();
	expect(res.headers).toMatchSnapshot();
	expect(render.mock.calls).toMatchSnapshot();
});

test('Auth controller POST invalid content type', async()=>{
	const render = jest.fn();
	const htmlTemplate = jest.fn((props, body)=>res=>render(res, body));
	const setToken = jest.fn();

	const getAuthController = AuthControllerFactory({ htmlTemplate, setToken });

	const auth = {
		authenticate: jest.fn(()=>false)
	};

	const req = {
		method: 'POST',
		async *[Symbol.asyncIterator](){
			yield 'password=123'
		},
		contentType: 'text/plain'
	};

	const controller = getAuthController(auth, req);

	const res = new MockRes;

	await expect(controller(res)).rejects.toBeInstanceOf(UnsupportedMediaTypeError)

	expect(auth.authenticate).not.toHaveBeenCalled();
	expect(setToken).not.toHaveBeenCalled();
	expect(render).not.toHaveBeenCalled();;
});