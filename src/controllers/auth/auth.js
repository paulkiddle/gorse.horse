import { parse } from '../../views/auth.js';
import authForm from '../../views/auth.js';
import deserializeForm from './deserializer/form.js';

// 415
export class UnsupportedMediaTypeError extends Error {}

export default function AuthControllerFactory({ htmlTemplate, setToken }) {
	return function getAuthController(auth, req){
		const { user, contentType } = req;
		
		return async function authController(res){
			let error = '';

			if(req.method === 'POST') {
				if(contentType && (contentType !== deserializeForm.type)) {
					throw new UnsupportedMediaTypeError(`POST data must use ${deserializeForm.type}, not ${contentType}.`);
				}
				const payload = parse(await deserializeForm(req));
				const token = await auth.authenticate(payload);

				if(token) {
					setToken(res, token);
					res.setHeader('Location', '/');
					res.statusCode = 303;
					res.end(token);
					return;
				}

				error = 'The password was incorrect.';
				res.statusCode = 401;
			}

			const render = htmlTemplate({ user, title: 'Log in' }, authForm(error));
			await render(res);
		};
	};
}