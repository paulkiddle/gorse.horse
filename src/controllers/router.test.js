import { expect, jest, test } from "@jest/globals";
import MockRes from "../res.mock.js";
import Auth from "../resources/auth.js";
import Gorse from "../resources/gorse.horse.js";
import RemoteForm from "../resources/remote-form.js";
import ControllerFactory, { NotImplementedError } from "./router.js";

test('Controller router', async ()=>{
	const render = jest.fn();
	const getController = ControllerFactory({ htmlTemplate: ()=>render, setToken: jest.fn() });
	const gorse = Object.create(Gorse.prototype);
	const auth = Object.create(Auth.prototype);
	const other = {};

	const gc = getController(gorse, {});
	expect(gc.name).toBe('defaultController');
	const res = new MockRes;
	await gc({}, res);
	expect(res).toMatchSnapshot();

	expect(getController(auth, {}).name).toBe('authController');
	expect(() => getController(other, {}).name).toThrow(NotImplementedError);
	expect(getController.error(new Error('Test Error'), {}).name).toBe('errorController')
})

test('Routes remote-form', async()=>{
	const model = new RemoteForm(jest.fn());

	const getController = ControllerFactory({});

	expect(getController(model, {}).name).toBe('remoteResourceController');
})