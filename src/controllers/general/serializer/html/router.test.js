import { expect, test } from '@jest/globals';
import Errors from '../../../../resources/errors.js';
import Gorse from '../../../../resources/gorse.horse.js';
import getBody from './router.js';

const rendered = async ({ body, ...args }) => ({ body: await body.render(), ...args })

test('Get body', async()=>{
	const gorse = new Gorse('http://example.com', { avatar: './avatar.png' });

	expect(await getBody(gorse)).toMatchSnapshot();

	const e =[];
	const errors = new Errors('User', true, e);

	expect(await rendered(await getBody(errors))).toMatchSnapshot();

	e.push({ stack: 'stack', user: 'User', time: '2021' });

	expect(await rendered(await getBody(errors))).toMatchSnapshot();

	expect(await getBody(null)).toBe(undefined);
})
