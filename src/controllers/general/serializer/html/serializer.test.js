import DefaultExport, { HtmlSerializer } from './serializer.js';
import { expect, jest } from '@jest/globals';
import html from 'encode-html-template-tag';

test('Default export', ()=>{
	expect(new DefaultExport({}, {})).toBeInstanceOf(HtmlSerializer);
})

test('Html Serializer', async ()=>{
	const body = {
		title: 'Title',
		body: 'body'
	};
	const getBody = jest.fn(()=>body)

	const res = {
		end: jest.fn()
	};
	const user = {
		isAnon: true
	}

	const serializer = new HtmlSerializer({ getBody, htmlTemplate: ({ title, user }, body) => async res => res.end(await html`<body>\n${user.isAnon}\n<h1>${title}</h1>\n${body}\n</body>`.render()) });
	const resource = Symbol('resource')
	await serializer.serialize(res, resource, user);

	expect(getBody).toHaveBeenCalledWith(resource);
	expect(res.end.mock.calls).toMatchSnapshot();

	user.isAnon = false;
	await serializer.serialize(res, resource, user);

	expect(res.end.mock.calls.slice(1)).toMatchSnapshot();
})