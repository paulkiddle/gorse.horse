import Gorse from '../../../../resources/gorse.horse.js';
import html from 'encode-html-template-tag';
import Errors from '../../../../resources/errors.js';

export default function getBody(resource) {
	if(resource instanceof Gorse) {
		return {
			title: resource.name,
			body: html`<h1>${resource.name}</h1>${resource.about}<img src="${resource.avatar}">`
		};
	}
	if(resource instanceof Errors) {
		const m = resource.empty ? html`Nothing to see here` : resource.map(e=>html`User: ${e.user}<br>${e.time}<pre>${e.stack}\n</pre><hr>`);

		return {
			title: 'Error Log',
			body: html`<h1>Error Log</h1> ${m}`
		};
	}
}