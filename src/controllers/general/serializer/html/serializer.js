import getBody from './router.js';

export const htmlTypes = ['text/html'];

export class HtmlSerializer {
	#getBody;
	#htmlTemplate;

	constructor({ getBody, htmlTemplate }) {
		this.#getBody = getBody;
		this.#htmlTemplate = htmlTemplate;
	}

	async serialize(res, resource, user) {
		const { title, body } = await this.#getBody(resource);

		const render = this.#htmlTemplate({ title, user }, body);

		await render(res);
	}
}

export default class extends HtmlSerializer {
	constructor({ ...etc }, user) {
		super({ ...etc, getBody }, user);
	}
}