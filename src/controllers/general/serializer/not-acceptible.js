export default class NotAcceptableError extends Error {
	constructor(message, types, ...etc) {
		super(message, ...etc);

		this.types = types;
	}
}