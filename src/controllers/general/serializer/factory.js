import HtmlSerializer, { htmlTypes } from './html/serializer.js';
import ActivitySerializer from './activity/activity.js';
import NotAcceptableError from './not-acceptible.js';

const activityTypes = [
	'application/activity+json',
	'application/json',
	'application/ld+json'
];

const types = [...htmlTypes, ...activityTypes];

export default function SerializerFactory({ htmlTemplate, compact }) {
	return function getSerializer(accepts){
		const type = accepts?.type(types) ?? types[0];

		if(htmlTypes.includes(type)) {
			return new HtmlSerializer({ htmlTemplate });
		}
	
		if(activityTypes.includes(type)) {
			return new ActivitySerializer({ compact });
		}

		throw new NotAcceptableError('The server doesn\'t know how to render this resource as any of the requested types.', types);
	};
}