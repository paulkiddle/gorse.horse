import Gorse from '../../../../resources/gorse.horse.js';
import ActivitySerializer, {fetch} from './activity.js';
import { expect, jest, test } from '@jest/globals';
import { NotImplementedError } from '../../../router.js';

async function runSerializer(payload, compact=v=>v){
	let body = '';
	const headers = Object.create(null);

	const res = {
		end: jest.fn((o='')=>{body+=o}),
		setHeader: jest.fn((k, v)=>{headers[k]=v}),
		write: jest.fn(o=>{body+=o})
	}

	const s = new ActivitySerializer({ compact });

	await s.serialize(res, payload);

	expect(res.end).toHaveBeenCalled();

	const { statusCode, statusText } = res;
	return {
		statusCode,
		statusText,
		headers,
		body
	};
}

test('Activity serializer', async()=>{
	const gorse = new Gorse('http://gorse.example/@gorse');

	expect(await runSerializer(gorse)).toMatchSnapshot();
})

test('False fetch', async ()=>{
	expect(fetch()).rejects.toBeInstanceOf(Error);
})

test('Not implemented', async()=>{
	await expect(runSerializer(null)).rejects.toBeInstanceOf(NotImplementedError);
})