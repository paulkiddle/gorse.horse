import Gorse from '../../../../resources/gorse.horse.js';
import { NotImplementedError } from '../../../router.js';

const activityJson = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"';

const AS = 'https://www.w3.org/ns/activitystreams';
const SEC = 'https://w3id.org/security/v1';
const as = ([k]) => `${AS}#${k}`;
const sec = ([k]) => `https://w3id.org/security#${k}`;
const ldp = ([k]) => `http://www.w3.org/ns/ldp#${k}`;

export default class ActivitySerializer {
	#compact;

	constructor({ compact }) {
		this.#compact = compact;
	}

	async serialize(res, resource) {
		const { data, context } = this.#getData(resource);

		res.setHeader('content-type', activityJson);
		const compacted = await this.#compact(data, context);
		const json = JSON.stringify(compacted);
		res.write(json);
		res.end();
	}

	#getData(resource){
		if(resource instanceof Gorse) {
			return {
				context: [AS, SEC],
				data: {
					'@id': resource.id,
					'@type': as`Person`,			
					[as`preferredUsername`]: resource.username,
					[as`name`]: resource.name,
					[as`summary`]: resource.about,
					[as`icon`]: {
						'@type': as`Image`,
						[as`url`]: { '@id': resource.avatar },
						[as`mediaType`]: resource.avatarType
					},
					[sec`publicKey`]: {
						'@id': resource.keyId,
						[sec`owner`]: { '@id': resource.id },
						[sec`publicKeyPem`]: resource.publicKey
					},
					[ldp`inbox`]: { '@id': resource.id + '/inbox' },
					[as`outbox`]: { '@id': resource.id + '/outbox' }
				}
			};
		}

		throw new NotImplementedError('Don\'t know how to render that resource as JSON');
	}
}

export async function fetch(url){
	throw new Error(`Fetch forbidden for context ${url}.`);
}
