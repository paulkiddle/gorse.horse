import accepts from 'accepts';
import ActivitySerializer from './activity/activity.js';
import GetSerializer from './factory.js';
import { default as NotAcceptableError } from './not-acceptible.js'
import HtmlSerializer from './html/serializer.js';

test('Gets HtmlSerializer', ()=>{
	const req = accepts({ headers: { acceot: 'text/html' } });
	const getSerializer = GetSerializer({});
	expect(getSerializer(req)).toBeInstanceOf(HtmlSerializer);
})

test('Gets ActivitySerializer', ()=>{
	const req = accepts({
		headers: {
			accept: 'application/json'
		}
	})
	const getSerializer = GetSerializer({});
	expect(getSerializer(req)).toBeInstanceOf(ActivitySerializer);
})

test('Throws error', ()=>{
	const req = accepts({
		headers: {
			accept: 'image/*'
		}
	})
	const getSerializer = GetSerializer({});
	expect(()=>getSerializer(req)).toThrow(NotAcceptableError);
})