import SerializerFactory from './serializer/factory.js';

export default function DefaultController({ htmlTemplate, compact }) {
	const getSerializer = SerializerFactory({ htmlTemplate, compact });

	return function getDefaultController(resource, { accepts, user }) {
		const serializer = getSerializer(accepts);
		return async function defaultController (res) {
			await serializer.serialize(res, resource, user);
		};
	};
}

DefaultController.prototype = Function.prototype;