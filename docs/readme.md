# New Web

Recently I decided to [move away from a static-generated blog](https://blog.mrkiddle.co.uk/my-first-blog).
But I also can't stand any of the one-size-fits-all platforms or frameworks.
I think they're a huge time sink and make your project more susceptible to code rot.
In fact the most fun I've had when running a blog is when I rolled my own in PHP when I was a baby developer.
So I decided to go back to that, with many more years of wisdom under my belt.

This tempalte is a basis to build my blog and other dynamic sites on top of.

## Principles

### Composition over Configuration

Use UNIX philosophy. My site is specific to my needs, but it shares common behaviour with other people's sites.
So instead of creating a platform that tries to meet everyone's needs, I am combining libraries
that solve more specific problems; how to route a request, how to fetch content, how to render a template.
These smaller libraries have a broader usefulness, and I don't have to write a load of configuration code or try to predict what users want to do.

Also, try not to make assumptions about the user's environment. If you need a database connection, make the use connect to it and pass in the query executor function.

### No transpilers or task runners

I've wasted so much of my life configuring webpack, babel, gulp, grunt, et al. But these tools are completely
unneccesary with a modern stack.

 - Node 14 supports ES Modules, so I don't need webpack.
 - It also supports many other recent ES language features, so I don't need babel
 - Instead of task runners I can use bash scripts and javascript files
 - And if I use progressive enhancement I don't need to polyfill anything on the front-end

Speaking of front end...

### Use vanilla Javascript for the front-end

And only when strictly needed.

 - No dynamic routers. The browser already knows how to do page navigation
 - No rendering libraries. HTML forms can handle user input and WebComponents are mature enough for anything else.

### Write specific code first

Write code that solves your particular use case. If you find yourself writing similar code in other places
or other projects, it might then be time to generalise it into an external library. But don't do it too early.

## Architectural principles

### Views

Views use the [`encode-html-template-tag`](https://www.npmjs.com/package/encode-html-template-tag) library
to ensure secure HTML with minimal effort.

```javascript
import html from 'encode-html-template-tag';

export default function userInfo({name, avatar='/'+name+'.jpg', url='/@'+name}) {
	return html`<div class="userInfo">
		<a href="${url}">
			<img src="${avatar}">
			${name}
		</a>
	</div>`;
}
```

### Forms

In the same file that you define your form, you should also define how to parse that form data once it's been submitted.

This is a sensible move, since if you change how your form is structured you will also need to change how it's parsed -
both of these things are in fact a [single responsibility](https://en.wikipedia.org/wiki/Single-responsibility_principle).

```javascript
import html from 'encode-html-template-tag';

export default initialValue =>
	// Define the form structure
	html`<form method="POST">
	<label>
		What's your name?
		<input name="name" value="${initialValue}">
	</label>
	<button>Submit</button>
	</form>
	`;

export const parse =
	// Define how we get the data from the request
	params => params.get('name');
```

### Routes

We implement a route as a function that accepts a request and returns a response.

In our specific case, we also pass the logged-in user.

The response object is either a view or a function that modifies Node's HTTP Response object, or both.

```javascript
import nameForm from './nameForm.js';
import userInfo from './userInfo.js';

export default async function(req, user){
	const form = nameForm(user.name);

	if(req.method === 'POST') {
		const name = await form.parse(request);

		return res => {
			res.setHeader('x-example-header', name);
			return userInfo(name);
		}
	}

	return form;
}
```

We can then create a super router that can do our URL routing and call these functions.

```javascript
import nameRoute from './nameRoute.js';
// Pretend we already know how to get users
import getUser from './users.js';

export default async function (req, res) {
	switch(req.url) {
		case '/':
			res.end('Home page');
			break;
		case '/name':
			const user = await getUser(req);
			const response = await nameRoute(req, user);
			const view = (response instanceof Function) ? response(res) : response;
			const output = await view.render();

			res.end(output)
	}
}

```

### Authentication

I don't want to implement my own auth mechanism, so I'm farming it out to mastodon using [masto-id-connect](https://www.npmjs.com/package/masto-id-connect).
