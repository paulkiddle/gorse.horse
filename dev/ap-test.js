import { Request, Response } from 'node-fetch';
import CustomFetch from 'custom-fetch';
import SignedFetch from 'signed-fetch';
import testKeys from './keys.js';

export default function apTester(fetch){
	const ownerId = 'https://profile.test/'
	const testKeyId = ownerId + '#main-key';
	const fetchWithTestProfile = new CustomFetch(req => {
		if(req.url === testKeyId) {
			return new Response(
				JSON.stringify({
					'https://w3id.org/security#publicKey': {
						'@id': testKeyId,
						'https://w3id.org/security#owner': { '@id': ownerId },
						'https://w3id.org/security#publicKeyPem': {
							'@value':  testKeys.public
						}
					}
				}),
				{ headers: { 'content-type': 'application/ld+json' } }
			);
		}

		return fetch(req);
	},{
		Request,
		Response
	});

	const signedFetch = new SignedFetch({ keyId: testKeyId, privateKey: testKeys.private }, { Request, Response, fetch });

	async function testUi(req, res){
		const { searchParams } = new URL(req.url, 'file://');
		const u = searchParams.get('url');
		let b = '';
		if(u){
			const r = await signedFetch(u, { headers: { accept: 'application/ld+json' } });
			b = await r.text();
			try {
				b = JSON.stringify(JSON.parse(b), null, 4);
			}catch(e) {

			}
		}
		res.end(`<form><input name="url"><button>Ok</button><pre>${b}`)
	}

	function wrapApp(fn) {
		return (req, res) => {
			if(req.headers.host.split(':')[0] === 'test.localhost'){
				return testUi(req, res)
			}	
			return fn(req, res);
		}
	}

	wrapApp.fetch = fetchWithTestProfile;

	return wrapApp;
}